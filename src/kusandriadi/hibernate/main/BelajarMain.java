package kusandriadi.hibernate.main;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class BelajarMain extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public BelajarMain(){
		super("Belajar Hibernate");
		setSize(660, 410);
		Dimension tengahDimension = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((tengahDimension.width - getWidth())/2, (tengahDimension.height - getHeight())/2);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		add(new BelajarPanel());
		setVisible(true);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try{
		UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		}catch(ClassNotFoundException e){
			
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		new BelajarMain();
	}

}
