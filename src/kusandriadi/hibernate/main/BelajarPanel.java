package kusandriadi.hibernate.main;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.table.TableModel;

import kusandriadi.hibernate.dao.hibernate.MahasiswaDaoHibernate;
import kusandriadi.hibernate.entity.Mahasiswa;

import com.toedter.calendar.JDateChooser;

public class BelajarPanel extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JButton btnSave = new JButton("Save"), btnClear = new JButton(
			"Clear"), btnUpdate = new JButton("Update"),
			btnDelete = new JButton("Delete");

	private JLabel lblNim = new JLabel("NIM : "), lblNama = new JLabel(
			"Nama : "), lblTTL = new JLabel("TTL : "), lblJur = new JLabel(
			"Jurusan : "), lblJenkel = new JLabel("Jenis Kelamin : "),
			lblAlamat = new JLabel("Alamat : "), lblRead = new JLabel(
					"*) klik 2x pada table untuk read data");

	private JTextArea arAlamat = new JTextArea(5, 30);

	private JTable tblMahasiswa = new JTable();

	private JScrollPane arScroll = new JScrollPane(arAlamat,
			ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
			ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER),
			tblScroll = new JScrollPane(tblMahasiswa,
					ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
					ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

	private JTextField txtNIM = new JTextField(), txtNama = new JTextField(),
			txtTmpt = new JTextField();

	private String Jurusan[] = { "--Pilih Jurusan--", "FTI", "FE", "FISIP" };

	private String Kelamin[] = { "--Jenis Kelamin--", "Pria", "Wanita" };

	private JComboBox cmbJurusan = new JComboBox(Jurusan),
			cmbJenkel = new JComboBox(Kelamin);

	private JDateChooser dateChooser = new JDateChooser();

	private SimpleDateFormat tanggal = new SimpleDateFormat("yyyy-MM-dd"),
			ubah = new SimpleDateFormat("dd-MM-yyyy");

	private MahasiswaDaoHibernate mdh = new MahasiswaDaoHibernate();

	private Mahasiswa mahasiswa = new Mahasiswa();

	private static String simpan = new String();

	public static String getSimpan() {
		return simpan;
	}

	public static void setSimpan(String simpan) {
		BelajarPanel.simpan = simpan;
	}

	public BelajarPanel() {
		setLayout(null);
		// mengatur label

		start();
		updateTable();
		autoNumber();
		lblNim.setBounds(20, 20, 90, 25);
		lblNama.setBounds(20, 60, 90, 25);
		lblTTL.setBounds(20, 100, 90, 25);
		lblJur.setBounds(20, 140, 90, 25);
		lblJenkel.setBounds(20, 180, 90, 25);
		lblAlamat.setBounds(20, 220, 90, 25);
		lblRead.setBounds(390, 20, 250, 25);

		// mengatur alignment label
		lblNim.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNama.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTTL.setHorizontalAlignment(SwingConstants.RIGHT);
		lblJur.setHorizontalAlignment(SwingConstants.RIGHT);
		lblJenkel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAlamat.setHorizontalAlignment(SwingConstants.RIGHT);

		lblNim.setFont(new Font(getFont().getName(), Font.BOLD, getFont()
				.getSize()));
		lblNama.setFont(new Font(getFont().getName(), Font.BOLD, getFont()
				.getSize()));
		lblTTL.setFont(new Font(getFont().getName(), Font.BOLD, getFont()
				.getSize()));
		lblJur.setFont(new Font(getFont().getName(), Font.BOLD, getFont()
				.getSize()));
		lblJenkel.setFont(new Font(getFont().getName(), Font.BOLD, getFont()
				.getSize()));
		lblAlamat.setFont(new Font(getFont().getName(), Font.BOLD, getFont()
				.getSize()));
		lblRead.setFont(new Font(getFont().getName(), Font.BOLD, getFont()
				.getSize()));

		// mengatur textfield
		txtNIM.setBounds(120, 20, 140, 25);
		txtNama.setBounds(120, 60, 140, 25);
		txtTmpt.setBounds(120, 100, 80, 25);

		// mengatur datechooser
		dateChooser.setLocale(new Locale("ID"));
		dateChooser.setCalendar(Calendar.getInstance());
		dateChooser.setDateFormatString("dd MMM yyyy");
		dateChooser.setBounds(210, 100, 130, 25);

		// mengatur cmbJur
		cmbJurusan.setBounds(120, 140, 130, 25);

		// mengatur jenkel
		cmbJenkel.setBounds(120, 180, 130, 25);

		// mengatur text area
		arScroll.setBounds(120, 220, 250, 90);
		arAlamat.setLineWrap(true);
		arAlamat.setWrapStyleWord(true);

		// mengatur table
		tblScroll.setBounds(390, 50, 250, 300);
		tblMahasiswa.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					TableModel model1 = tblMahasiswa.getModel();
					simpan = model1
							.getValueAt(tblMahasiswa.getSelectedRow(), 0)
							.toString();
					mdh.moveDataFromTable();
					btnSave.setEnabled(false);
					btnUpdate.setEnabled(true);
					btnDelete.setEnabled(true);
					isi();
				}
			}

		});

		// mengatur button
		btnClear.setBounds(20, 330, 80, 25);
		btnSave.setBounds(110, 330, 80, 25);
		btnDelete.setBounds(200, 330, 80, 25);
		btnUpdate.setBounds(290, 330, 80, 25);

		btnClear.addActionListener(this);
		btnSave.addActionListener(this);
		btnDelete.addActionListener(this);
		btnUpdate.addActionListener(this);

		// menambah komponen ke panel
		// menambah label
		add(lblNim);
		add(lblNama);
		add(lblTTL);
		add(lblJur);
		add(lblJenkel);
		add(lblAlamat);
		add(lblRead);
		// menambah textfield
		add(txtNIM);
		add(txtNama);
		add(txtTmpt);
		// menambah datechooser
		add(dateChooser);
		// menambah combo
		add(cmbJurusan);
		add(cmbJenkel);
		// menambah text area
		add(arScroll);
		// menambah table
		add(tblScroll);
		// menambah button
		add(btnClear);
		add(btnSave);
		add(btnDelete);
		add(btnUpdate);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == btnSave) {
			if (cek()) {
				JOptionPane.showMessageDialog(null,
						"Maaf, data yang anda input kurang lengkap!");
			} else {
				takeData();

				mdh.save(mahasiswa);
				updateTable();
				start();
				clear();
				autoNumber();
				JOptionPane.showMessageDialog(null, "Data Berhasil Diinput!");
			}
		}
		if (obj == btnClear) {
			clear();
		}
		if (obj == btnDelete) {
			mdh.setNim(txtNIM.getText());
			mdh.delete();
			updateTable();
			pengaturan();
		}
		if (obj == btnUpdate) {
			if (cek()) {
				JOptionPane.showMessageDialog(null,
						"Maaf, data yang anda input kurang lengkap!");
			} else {
				mdh.setNim(txtNIM.getText());
				mdh.setNama(txtNama.getText());
				mdh.setTempat(txtTmpt.getText());
				mdh.setTgl_lahir(ubah.format(dateChooser.getDate()));
				mdh.setJurusan((String) cmbJurusan.getSelectedItem());
				mdh.setJenkel((String) cmbJenkel.getSelectedItem());
				mdh.setAlamat(arAlamat.getText());
				mdh.update();
				updateTable();
				pengaturan();
			}
		}
	}

	private void takeData() {
		mahasiswa.setNim(txtNIM.getText());
		mahasiswa.setNama(txtNama.getText());
		mahasiswa.setTmp_lahir(txtTmpt.getText());
		mahasiswa.setTgl_lahir(dateChooser.getDate());
		mahasiswa.setJenkel(cmbJenkel.getSelectedItem().toString());
		mahasiswa.setJurusan(cmbJurusan.getSelectedItem().toString());
		mahasiswa.setAlamat(arAlamat.getText());
	}

	private boolean cek() {
		if (txtNIM.getText().trim().equals("")
				|| (txtNama.getText().trim().equals(""))
				|| (txtTmpt.getText().trim().equals(""))
				|| (tanggal.format(dateChooser.getDate()).trim().equals(""))
				|| (cmbJurusan.getSelectedIndex() == 0)
				|| (cmbJenkel.getSelectedIndex() == 0)
				|| (arAlamat.getText().trim().equals(""))) {
			return true;
		}
		return false;
	}

	private void start() {
		btnUpdate.setEnabled(false);
		btnDelete.setEnabled(false);
		txtNIM.setEnabled(false);
		btnSave.setEnabled(true);
	}

	private void clear() {
		txtNama.setText("");
		txtTmpt.setText("");
		dateChooser.setCalendar(Calendar.getInstance());
		cmbJurusan.setSelectedIndex(0);
		cmbJenkel.setSelectedIndex(0);
		arAlamat.setText("");
		btnUpdate.setEnabled(false);
		btnDelete.setEnabled(false);
	}

	private void isi() {
		txtNIM.setText(mdh.getNim());
		txtNama.setText(mdh.getNama());
		txtTmpt.setText(mdh.getTempat());
		try {
			dateChooser.setDate(ubah.parse(mdh.getTgl_lahir()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cmbJurusan.setSelectedItem(mdh.getJurusan());
		cmbJenkel.setSelectedItem(mdh.getJenkel());
		arAlamat.setText(mdh.getAlamat());
	}

	private void updateTable() {
		TableModel model = mdh.updateTable();
		tblMahasiswa.setModel(model);
	}

	private void autoNumber() {
		String auto = mdh.autoNumber();
		if (auto == null) {
			txtNIM.setText("M0001");
		} else {
			auto = auto.substring(1);
			int angka = Integer.parseInt(auto);
			angka = angka + 1;
			if (angka <= 9) {
				auto = "000" + angka;
			} else if (angka >= 10 && angka <= 99) {
				auto = "00" + angka;
			} else if (angka >= 100 && angka <= 999) {
				auto = "0" + angka;
			} else if (angka >= 1000 && angka <= 9999) {
				auto = "" + angka;
			}
			if (auto.length() > 5) {
				auto = auto.substring(auto.length() - 5, auto.length());
			}
			auto = "M" + auto;
			txtNIM.setText(auto);
		}
	}

	private void pengaturan() {
		start();
		clear();
		autoNumber();
	}
}
