package kusandriadi.hibernate.dao;

import javax.swing.table.TableModel;

import kusandriadi.hibernate.entity.Mahasiswa;

public interface MahasiswaDao {
	
	public void save(Mahasiswa mahasiswa);
	
	public void delete();
	
	public void update();
	
	public TableModel updateTable();
	
	public void moveDataFromTable();
	
	public String autoNumber();
	
}
