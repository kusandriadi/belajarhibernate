package kusandriadi.hibernate.dao.hibernate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.TableModel;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import kusandriadi.hibernate.dao.MahasiswaDao;
import kusandriadi.hibernate.entity.Mahasiswa;
import kusandriadi.hibernate.main.BelajarPanel;
import kusandriadi.hibernate.tablemodel.TableMahasiswaModel;
import kusandriadi.hibernate.util.HibernateUtil;

public class MahasiswaDaoHibernate implements MahasiswaDao {

	private String nim = new String(), nama = new String(),
			tempat = new String(), tgl_lahir = new String(),
			jurusan = new String(), jenkel = new String(),
			alamat = new String();

	private SimpleDateFormat tanggal = new SimpleDateFormat("dd-MM-yyyy");

	public String getNim() {
		return nim;
	}

	public void setNim(String nim) {
		this.nim = nim;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getTempat() {
		return tempat;
	}

	public void setTempat(String tempat) {
		this.tempat = tempat;
	}

	public String getJurusan() {
		return jurusan;
	}

	public void setJurusan(String jurusan) {
		this.jurusan = jurusan;
	}

	public String getJenkel() {
		return jenkel;
	}

	public void setJenkel(String jenkel) {
		this.jenkel = jenkel;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getTgl_lahir() {
		return tgl_lahir;
	}

	public void setTgl_lahir(String tglLahir) {
		tgl_lahir = tglLahir;
	}

	public MahasiswaDaoHibernate() {

	}

	@Override
	public void delete() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();

			int hqlDelete = session.createQuery(
					"delete Mahasiswa where nim = '" + getNim() + "'")
					.executeUpdate();

			if (hqlDelete == 1) {
				updateTable();
				JOptionPane.showMessageDialog(null, "Data berhasil di delete!");
			}

			session.getTransaction().commit();
		} catch (HibernateException e) {
			session.getTransaction().rollback();
		}
		session.close();
	}

	@Override
	public void save(Mahasiswa mahasiswa) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();

			session.save(mahasiswa);

			updateTable();

			session.getTransaction().commit();
		} catch (HibernateException e) {
			session.getTransaction().rollback();
		}
		session.close();

	}

	@Override
	public void update() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();

			String hqlUpdate = "update Mahasiswa m set m.nama = :nama,"
					+ "m.tmp_lahir = :tmp_lahir," + "m.tgl_lahir = :tgl_lahir,"
					+ "m.jurusan = :jurusan," + "m.jenkel = :jenkel,"
					+ "m.alamat = :alamat" + " where m.nim = :nim";

			int updateEntity = session.createQuery(hqlUpdate).setString("nama",
					getNama()).setString("tmp_lahir", getTempat()).setDate(
					"tgl_lahir", tanggal.parse(getTgl_lahir())).setString(
					"jurusan", getJurusan()).setString("jenkel", getJenkel())
					.setString("alamat", getAlamat())
					.setString("nim", getNim()).executeUpdate();

			if (updateEntity == 1) {
				updateTable();
				JOptionPane.showMessageDialog(null, "Data Berhasil Diupdate");
			}

			session.getTransaction().commit();

		} catch (HibernateException e) {
			session.getTransaction().rollback();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		session.close();
	}

	@SuppressWarnings("unchecked")
	@Override
	public TableModel updateTable() {
		TableModel model;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();

			List<Object[]> objectResult = null;
			Query query = session
					.createQuery("select c.nim, c.nama from Mahasiswa c");
			objectResult = query.list();

			ArrayList<Mahasiswa> listmhs = new ArrayList<Mahasiswa>();

			for (Object[] resultElement : objectResult) {
				Mahasiswa mahasiswa = new Mahasiswa();
				mahasiswa.setNim((String) resultElement[0]);
				mahasiswa.setNama((String) resultElement[1]);
				listmhs.add(mahasiswa);
			}
			model = new TableMahasiswaModel(listmhs);
			session.getTransaction().commit();

		} catch (HibernateException e) {
			session.getTransaction().rollback();
			return null;
		}
		session.close();
		return model;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void moveDataFromTable() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();

			ArrayList<Mahasiswa> maha = new ArrayList<Mahasiswa>(session
					.createQuery(
							"from Mahasiswa where nim = '"
									+ BelajarPanel.getSimpan() + "'").list());

			for (Mahasiswa mhs : maha) {
				setNim(mhs.getNim());
				setNama(mhs.getNama());
				setTempat(mhs.getTmp_lahir());
				setTgl_lahir(tanggal.format(mhs.getTgl_lahir()));
				setJurusan(mhs.getJurusan());
				setJenkel(mhs.getJenkel());
				setAlamat(mhs.getAlamat());
			}
			session.getTransaction().commit();

		} catch (HibernateException e) {
			session.getTransaction().rollback();
		}
		session.close();
	}

	@SuppressWarnings("unchecked")
	@Override
	public String autoNumber() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();

			List objectResult = null;
			
			objectResult = session.createQuery("select max(nim) as nim from Mahasiswa").list();
			
			for (Object resultElement : objectResult){
				setNim((String)resultElement);
			}

			session.getTransaction().commit();
		} catch (HibernateException e) {
			session.getTransaction().rollback();
			return null;
		}
		session.close();
		return getNim();
	}
}
