package kusandriadi.hibernate.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Mahasiswa")
public class Mahasiswa {

	@Id
	@Column(name = "nim", nullable = false, length = 10)
	private String nim;

	@Column(name = "nama")
	private String nama;

	@Column(name = "tmp_lahir")
	private String tmp_lahir;

	@Column(name = "tgl_lahir")
	private Date tgl_lahir;

	@Column(name = "jurusan")
	private String jurusan;

	@Column(name = "jenkel")
	private String jenkel;

	@Column(name = "alamat")
	private String alamat;

	public String getNim() {
		return nim;
	}

	public void setNim(String nim) {
		this.nim = nim;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getTmp_lahir() {
		return tmp_lahir;
	}

	public void setTmp_lahir(String tmpLahir) {
		tmp_lahir = tmpLahir;
	}

	public Date getTgl_lahir() {
		return tgl_lahir;
	}

	public void setTgl_lahir(Date tglLahir) {
		tgl_lahir = tglLahir;
	}

	public String getJurusan() {
		return jurusan;
	}

	public void setJurusan(String jurusan) {
		this.jurusan = jurusan;
	}

	public String getJenkel() {
		return jenkel;
	}

	public void setJenkel(String jenkel) {
		this.jenkel = jenkel;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alamat == null) ? 0 : alamat.hashCode());
		result = prime * result + ((jenkel == null) ? 0 : jenkel.hashCode());
		result = prime * result + ((jurusan == null) ? 0 : jurusan.hashCode());
		result = prime * result + ((nama == null) ? 0 : nama.hashCode());
		result = prime * result + ((nim == null) ? 0 : nim.hashCode());
		result = prime * result
				+ ((tgl_lahir == null) ? 0 : tgl_lahir.hashCode());
		result = prime * result
				+ ((tmp_lahir == null) ? 0 : tmp_lahir.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mahasiswa other = (Mahasiswa) obj;
		if (alamat == null) {
			if (other.alamat != null)
				return false;
		} else if (!alamat.equals(other.alamat))
			return false;
		if (jenkel == null) {
			if (other.jenkel != null)
				return false;
		} else if (!jenkel.equals(other.jenkel))
			return false;
		if (jurusan == null) {
			if (other.jurusan != null)
				return false;
		} else if (!jurusan.equals(other.jurusan))
			return false;
		if (nama == null) {
			if (other.nama != null)
				return false;
		} else if (!nama.equals(other.nama))
			return false;
		if (nim == null) {
			if (other.nim != null)
				return false;
		} else if (!nim.equals(other.nim))
			return false;
		if (tgl_lahir == null) {
			if (other.tgl_lahir != null)
				return false;
		} else if (!tgl_lahir.equals(other.tgl_lahir))
			return false;
		if (tmp_lahir == null) {
			if (other.tmp_lahir != null)
				return false;
		} else if (!tmp_lahir.equals(other.tmp_lahir))
			return false;
		return true;
	}

}
