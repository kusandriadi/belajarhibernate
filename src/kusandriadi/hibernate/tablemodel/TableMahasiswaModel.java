package kusandriadi.hibernate.tablemodel;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import kusandriadi.hibernate.entity.Mahasiswa;

public class TableMahasiswaModel extends AbstractTableModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	ArrayList<Mahasiswa> list;
	
	public TableMahasiswaModel(ArrayList<Mahasiswa> list){
		this.list = list;
	}
	
	@Override
	public int getColumnCount() {
		return 2;
	}

	@Override
	public int getRowCount() {
		return list.size();
	}

	@Override
	public Object getValueAt(int row, int column) {
		switch (column) {
		case 0:
			return list.get(row).getNim();
		case 1: 
			return list.get(row).getNama();
		default:
			return null;
		}
	}

	@Override
	public String getColumnName(int column) {
		switch (column) {
		case 0:
			return "NIM";
		case 1:
			return "Nama Mahasiswa";
		default:
			return null;
		}
	}
}
