﻿drop database if exists belajarMahasiswa;

create database belajarMahasiswa;

use belajarMahasiswa;

create table Mahasiswa(
nim varchar(5) primary key,
nama varchar(25),
tmp_lahir varchar(13),
tgl_lahir date,
jurusan varchar(6),
jenkel varchar(10),
alamat text
);
